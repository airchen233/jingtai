import Link from 'next/link'
import Layout from '../../components/layout'
import { useTranslation, useLanguageQuery } from 'next-export-i18n'
import styles from '../../styles/News.module.css'
import React, { useEffect, useState } from 'react'
import ReactPaginate from 'react-paginate'

const items = []

const Items = ({ currentItems }) => {
  const { t } = useTranslation()
  const [query] = useLanguageQuery()
  return (
    <div className={styles.item}>
      <ul>
        {currentItems.map((item, i) => (
          <Link href={{ pathname: '/news/' + item, query }} passHref key={i}>
            <a>
              <li>
                <span>{t(`news.${item}.date`)}</span>
                <span>{t(`news.${item}.title`)}</span>
              </li>
            </a>
          </Link>
        ))}
      </ul>
    </div>
  )
}

const PaginatedItems = ({ itemsPerPage }) => {
  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState([])
  const [pageCount, setPageCount] = useState(0)
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0)

  useEffect(() => {
    // Fetch items from another resources.
    const endOffset = itemOffset + itemsPerPage
    // console.log(`Loading items from ${itemOffset} to ${endOffset}`)
    setCurrentItems(items.slice(itemOffset, endOffset))
    setPageCount(Math.ceil(items.length / itemsPerPage))
  }, [itemOffset, itemsPerPage])

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % items.length
    // console.log(`User requested page number ${event.selected}, which is offset ${newOffset}`)
    setItemOffset(newOffset)
  }

  return (
    <>
      <Items currentItems={currentItems} />
      <ReactPaginate
        className={styles.pageContainer}
        pageClassName={styles.page}
        activeClassName={styles.pageSelected}
        disabledClassName={styles.pageDisabled}
        breakLabel="..."
        nextLabel=">"
        onPageChange={handlePageClick}
        pageRangeDisplayed={5}
        pageCount={pageCount}
        previousLabel="<"
        renderOnZeroPageCount={null}
      />
    </>
  )
}
const News = () => {
  const { t } = useTranslation()

  return (
    <Layout>
      <div className={styles.main}>
        <div className={styles.image}>
          <img
            src="/images/news/p1.png"
            alt="header"
            width={4800}
            data-aos="fade-down-right"
            data-aos-delay="0"
          />
          <img
            src="/images/home/p4.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="50"
          />
          <img
            src="/images/home/p2.png"
            alt="header"
            width={4800}
            data-aos="fade-down-right"
            data-aos-delay="100"
          />
          <img
            src="/images/home/p3.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="150"
          />
          <img
            src="/images/news/text.png"
            alt="about"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="200"
          />
          <img
            src="/images/news/left.png"
            alt="about"
            width={4800}
            data-aos="fade-right"
            data-aos-delay="200"
          />
        </div>
        <article className={styles.list} data-aos="fade-up" data-aos-delay="300">
          <h1>{t('menu.market.news')}</h1>
          <PaginatedItems itemsPerPage={5} />
        </article>
      </div>
    </Layout>
  )
}
export default News
