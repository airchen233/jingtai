import Layout from '../../components/layout'
import { useTranslation } from 'next-export-i18n'
import styles from '../../styles/Team.module.css'

const Team = () => {
  const { t } = useTranslation()
  return (
    <Layout>
      <div className={styles.main}>
        <div className={styles.image}>
          <img
            src="/images/team/p1.png"
            alt="header"
            width={4800}
            data-aos="fade-down-right"
            data-aos-delay="0"
          />
          <img
            src="/images/home/p4.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="50"
          />
          <img
            src="/images/home/p2.png"
            alt="header"
            width={4800}
            data-aos="fade-down-right"
            data-aos-delay="100"
          />
          <img
            src="/images/home/p3.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="150"
          />
          <img
            src="/images/team/text.png"
            alt="about"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="200"
          />
          <img
            src="/images/team/left.png"
            alt="about"
            width={4800}
            data-aos="fade-right"
            data-aos-delay="200"
          />
        </div>
        <article className={styles.about} data-aos="fade-up" data-aos-delay="300">
          <h1>{t('article.team.title')}</h1>
          <section>{t('article.team.section')}</section>
        </article>
      </div>
    </Layout>
  )
}
export default Team
