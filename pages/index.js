import Layout from '../components/layout'
import { useTranslation } from 'next-export-i18n'
import styles from '../styles/Home.module.css'

const Home = () => {
  const { t } = useTranslation()
  return (
    <Layout>
      <div className={styles.main}>
        <div className={styles.image}>
          <img
            src="/images/home/p1-2.png"
            alt="header"
            width={4800}
            data-aos="fade-down-right"
            data-aos-delay="0"
          />
          <img
            src="/images/home/p4.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="50"
          />
          <img
            src="/images/home/p2.png"
            alt="header"
            width={4800}
            data-aos="fade-down-right"
            data-aos-delay="100"
          />
          <img
            src="/images/home/p3.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="150"
          />
          <img
            src="/images/home/text.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="200"
          />
          <img
            src="/images/home/left.png"
            alt="about"
            width={4800}
            data-aos="fade-right"
            data-aos-delay="200"
          />
        </div>
        <article className={styles.slogan} data-aos="fade-up" data-aos-delay="300">
          <section>
            <img src="/images/home/slogan.png" alt="slogan" width={642} />
          </section>
          <section>{t('article.about.section')}</section>
        </article>
        <article className={styles.qrcode} data-aos="fade-up" data-aos-delay="300">
          <h3>{t('article.qrcode.title')}</h3>
          <img src="/images/home/qrcode.jpg" alt="slogan" width={428} />
        </article>
      </div>
    </Layout>
  )
}
export default Home
