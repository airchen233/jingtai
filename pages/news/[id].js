import { useRouter } from 'next/router'
import Layout from '../../components/layout'
import { useTranslation } from 'next-export-i18n'
import styles from '../../styles/Detail.module.css'

const Detail = () => {
  const { t } = useTranslation()
  const router = useRouter()
  const { id } = router.query

  return (
    <Layout>
      <div className={styles.main} data-aos="zoom-out-down">
        <div className={styles.image}>
          <img
            src="/images/detail/p1.png"
            alt="header"
            width={3753}
            data-aos="fade-down-right"
            data-aos-delay="0"
          />
          <img
            src="/images/detail/p2.png"
            alt="header"
            width={1698}
            data-aos="fade-down"
            data-aos-delay="500"
          />
        </div>
        <article className={styles.detail} data-aos="fade-up" data-aos-delay="1000">
          <h1>{t(`news.${id}.title`)}</h1>
          <h2>{t(`news.${id}.date`)}</h2>
          <section
            dangerouslySetInnerHTML={{
              __html: t('news.template', { data: t(`news.${id}.content`) }),
            }}></section>
        </article>
      </div>
    </Layout>
  )
}

export default Detail
