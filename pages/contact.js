import Layout from '../components/layout'
import { useTranslation } from 'next-export-i18n'
import styles from '../styles/Contact.module.css'

const Contact = () => {
  const { t } = useTranslation()
  return (
    <Layout>
      <div className={styles.main}>
        <div className={styles.image}>
          <img
            src="/images/contact/p1.png"
            alt="header"
            width={4800}
            data-aos="fade-down-right"
            data-aos-delay="0"
          />
          <img
            src="/images/home/p4.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="50"
          />
          <img
            src="/images/home/p2.png"
            alt="header"
            width={4800}
            data-aos="fade-down-right"
            data-aos-delay="100"
          />
          <img
            src="/images/home/p3.png"
            alt="header"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="150"
          />
          <img
            src="/images/contact/text.png"
            alt="about"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="200"
          />
          <img
            src="/images/contact/right.png"
            alt="about"
            width={4800}
            data-aos="fade-down"
            data-aos-delay="200"
          />
          <img
            src="/images/contact/bottom.png"
            alt="about"
            width={4800}
            data-aos="fade-up"
            data-aos-delay="200"
          />
          <img
            src="/images/contact/bg.png"
            alt="about"
            width={4800}
            data-aos="fade-up"
            data-aos-delay="200"
          />
        </div>
        <article className={styles.contact} data-aos="fade-up" data-aos-delay="300">
          <h1>{t('menu.contact.contact')}</h1>
          <ul>
            <li>
              <img src="/icons/location.png" alt="logo" width={24} height={40} />
              <span>{t('article.contact.section1')}</span>
            </li>
            <li>
              <img src="/icons/mail.png" alt="logo" width={40} height={32} />
              <span>{t('article.contact.section2')}</span>
            </li>
            <li>
              <img src="/icons/mobile.png" alt="logo" width={24} />
              <span>{t('article.contact.section3')}</span>
            </li>
          </ul>
        </article>
        <article className={styles.work} data-aos="fade-up">
          <h1>{t('menu.contact.joinus')}</h1>
          <ul>
            <li>
              <img src="/icons/work.png" alt="logo" width={40} />
              <span>{t('article.contact.section2')}</span>
            </li>
          </ul>
        </article>
        <article className={styles.bp} data-aos="fade-up">
          <h1>{t('menu.contact.bp')}</h1>
          <ul>
            <li>
              <img src="/icons/bp.png" alt="logo" width={40} />
              <span>{t('article.contact.section4')}</span>
            </li>
          </ul>
        </article>
      </div>
    </Layout>
  )
}
export default Contact
