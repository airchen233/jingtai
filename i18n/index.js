var en = require('./translations.en.json')
var zh = require('./translations.zh.json')

const i18n = {
  translations: {
    en: en,
    zh: zh,
  },
  defaultLang: 'zh',
}

module.exports = i18n
