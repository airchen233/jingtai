import Link from 'next/link'
import styles from '../styles/Footer.module.css'

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.footerContainer}>
        <p>Copyright © 2021 jt-capital.com.cn All Rights Reserved 版权所有: 竞泰资本</p>
        <p>
          <Link href="https://beian.miit.gov.cn/">粤ICP备2022003949号-1</Link>
        </p>
      </div>
    </footer>
  )
}
export default Footer
