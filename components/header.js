import React from 'react'

import Link from 'next/link'
import Menu from '../components/menu'
import Locale from './locale'
import styles from '../styles/Header.module.css'
import { useLanguageQuery } from 'next-export-i18n'

const Header = () => {
  const [query] = useLanguageQuery()

  return (
    <header className={styles.header} data-aos="fade-down" data-aos-delay="300">
      <div className={styles.headerContainer}>
        <div className={styles.logo}>
          <Link href={{ pathname: '/', query: query }} passHref>
            <a>
              <img src="/images/logo.png" alt="logo" width={502} height={157} />
            </a>
          </Link>
        </div>
        <div className={styles.menu}>
          <Menu />
          <Locale />
        </div>
      </div>
    </header>
  )
}
export default Header
