import Link from 'next/link'
import { useTranslation, useLanguageQuery } from 'next-export-i18n'
import { useRouter } from 'next/router'

const Menu = () => {
  const { t } = useTranslation()
  const [query] = useLanguageQuery()
  const router = useRouter()
  const route = [
    {
      key: 'menu.about.index',
      children: [
        {
          path: '/',
          key: 'menu.about.about',
        },
        {
          path: '/about/team',
          key: 'menu.about.team',
        },
      ],
    },
    {
      key: 'menu.market.index',
      children: [
        {
          path: '/market/activities',
          key: 'menu.market.activities',
        },
        {
          path: '/market/news',
          key: 'menu.market.news',
        },
      ],
    },
    {
      key: 'menu.contact.index',
      children: [
        {
          path: '/contact',
          key: 'menu.contact.contact',
        },
      ],
    },
  ]

  return route.map((item, index) => (
    <ul key={index}>
      <div>{t(item.key)}</div>

      {item.children.map((r, i) => (
        <li key={i}>
          <Link href={{ pathname: r.path, query }} active={r.path === router.pathname}>
            {t(r.key)}
          </Link>
        </li>
      ))}
    </ul>
  ))
}
export default Menu
