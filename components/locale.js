import { useTranslation, useLanguageQuery, LanguageSwitcher } from 'next-export-i18n'

const Locale = () => {
  const { t } = useTranslation()
  const [query] = useLanguageQuery()
  const lang = query ? (query.lang === 'en' ? 'zh' : 'en') : 'zh'

  return (
    <button>
      <LanguageSwitcher lang={lang}>{t('locale.en/zh')}</LanguageSwitcher>
    </button>
  )
}
export default Locale
